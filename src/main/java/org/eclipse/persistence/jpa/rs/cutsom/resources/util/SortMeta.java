package org.eclipse.persistence.jpa.rs.cutsom.resources.util;

import org.springframework.data.domain.Sort;

public class SortMeta {

	public String field;
	public int order;
	
	public Sort.Direction toSortDirection() {
        return order == 1 ? Sort.Direction.ASC : Sort.Direction.DESC;
    }
}
