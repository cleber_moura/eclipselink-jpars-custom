package org.eclipse.persistence.jpa.rs.cutsom.resources.util;

/**
 * 
 * @author ctm
 *
 */
public class FilterMetadata {

	public Object value;
	/**
	 * startsWith, contains, endsWith, equals, notEquals, in
	 */
	public String matchMode;
	
}