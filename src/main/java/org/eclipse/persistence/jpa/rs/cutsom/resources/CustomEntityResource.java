package org.eclipse.persistence.jpa.rs.cutsom.resources;

import static org.eclipse.persistence.jpa.rs.resources.common.AbstractResource.SERVICE_VERSION_FORMAT;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.internal.jpa.EJBQueryImpl;
import org.eclipse.persistence.internal.queries.ReportItem;
import org.eclipse.persistence.internal.sessions.AbstractSession;
import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.jpa.rs.PersistenceContext;
import org.eclipse.persistence.jpa.rs.QueryParameters;
import org.eclipse.persistence.jpa.rs.cutsom.resources.util.PageRequestByExample;
import org.eclipse.persistence.jpa.rs.exceptions.JPARSException;
import org.eclipse.persistence.jpa.rs.features.FeatureSet.Feature;
import org.eclipse.persistence.jpa.rs.features.fieldsfiltering.FieldsFilter;
import org.eclipse.persistence.jpa.rs.features.fieldsfiltering.FieldsFilteringValidator;
import org.eclipse.persistence.jpa.rs.features.paging.PagingResponseBuilder;
import org.eclipse.persistence.jpa.rs.resources.EntityResource;
import org.eclipse.persistence.jpa.rs.util.IdHelper;
import org.eclipse.persistence.jpa.rs.util.JPARSLogger;
import org.eclipse.persistence.jpa.rs.util.JTATransactionWrapper;
import org.eclipse.persistence.jpa.rs.util.ResourceLocalTransactionWrapper;
import org.eclipse.persistence.jpa.rs.util.StreamingOutputMarshaller;
import org.eclipse.persistence.jpa.rs.util.TransactionWrapper;
import org.eclipse.persistence.jpa.rs.util.list.PageableCollection;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.DirectToFieldMapping;
import org.eclipse.persistence.queries.DatabaseQuery;
import org.eclipse.persistence.queries.QueryByExamplePolicy;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.queries.ReportQuery;
import org.eclipse.persistence.queries.ReportQueryResult;
import org.eclipse.persistence.sessions.DatabaseSession;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Customização do {@link EntityResource} para paginação dos registros com
 * suporte a filtros.
 * 
 * @author clebertmoura
 *
 */
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/{version : " + SERVICE_VERSION_FORMAT + "}/{context}/entity/")
public class CustomEntityResource extends org.eclipse.persistence.jpa.rs.resources.EntityResource {

	private static final String CLASS_NAME = CustomEntityResource.class.getName();
	
	protected TransactionWrapper transaction;
	
	/**
	 * Método responsável por construir o {@link TransactionWrapper} utilizando o {@link PersistenceContext} informado.
	 * 
	 * @param context
	 * @return
	 */
	protected TransactionWrapper getTransactionWrapper(PersistenceContext context) {
		if (transaction == null) {
			if (context.getServerSession().hasExternalTransactionController()) {
	            transaction = new JTATransactionWrapper();
	        } else {
	            transaction = new ResourceLocalTransactionWrapper();
	        }
		}
		return transaction;
	}

	@POST
	@Path("{type}/paging")
	public Response findByExamplePaging(@PathParam("version") String version,
			@PathParam("context") String persistenceUnit, @PathParam("type") String type,
			PageRequestByExample<?> request, @Context HttpHeaders hh, @Context UriInfo ui) {
		setRequestUniqueId();
		return findByExamplePagingInternal(version, persistenceUnit, type, request, hh, ui);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Response findByExamplePagingInternal(String version, String persistenceUnit, String type,
			PageRequestByExample<?> request, HttpHeaders headers, UriInfo uriInfo) {
		JPARSLogger.entering(CLASS_NAME, "findByExamplePagingInternal", new Object[] { "POST", version, persistenceUnit,
				type, request, headers, uriInfo.getRequestUri().toASCIIString() });

		try {
			final PersistenceContext context = getPersistenceContext(persistenceUnit, type, uriInfo.getBaseUri(),
					version, null);
			final Map<String, String> discriminators = getMatrixParameters(uriInfo, persistenceUnit);
			ClassDescriptor descriptor = context.getServerSession().getDescriptorForAlias(type);
			if (descriptor == null) {
				JPARSLogger.error(context.getSessionLog(), "jpars_could_not_find_entity_type",
						new Object[] { type, persistenceUnit });
				throw JPARSException.classOrClassDescriptorCouldNotBeFoundForEntity(type, persistenceUnit);
			} else {
				Class<?> probeType = descriptor.getJavaClass();
				EntityManager em = context.getEmf().createEntityManager(discriminators);

				// Create a native EclipseLink query using QBE policy
				QueryByExamplePolicy policy = new QueryByExamplePolicy();
				policy.excludeDefaultPrimitiveValues();
				policy.addSpecialOperation(String.class, "likeIgnoreCase");

				ObjectMapper mapper = new ObjectMapper();
				Object convertValue = mapper.convertValue(request.example, probeType);
				ReadAllQuery q = new ReadAllQuery(convertValue, policy);
				
				if (request.lazyLoadEvent != null) {
					if (request.lazyLoadEvent.multiSortMeta != null && !request.lazyLoadEvent.multiSortMeta.isEmpty()) {
						request.lazyLoadEvent.multiSortMeta.forEach(sortMeta -> {
							if (sortMeta.toSortDirection().isAscending()) {
								q.addAscendingOrdering(sortMeta.field);
							} else {
								q.addDescendingOrdering(sortMeta.field);
							}
						});
					} else if (request.lazyLoadEvent.sortField != null && !request.lazyLoadEvent.sortField.isEmpty()) {
						if (request.lazyLoadEvent.toSortDirection().isAscending()) {
							q.addAscendingOrdering(request.lazyLoadEvent.sortField);
						} else {
							q.addDescendingOrdering(request.lazyLoadEvent.sortField);
						}
					}
				}
				
				ReportQuery rq = new ReportQuery(q.getExpressionBuilder());
				rq.addCount();
				rq.setExampleObject(convertValue);
				rq.setQueryByExamplePolicy(policy);
				
				Query queryCount = JpaHelper.createQuery(rq, em);
				Integer totalRegisters = (Integer) ((ReportQueryResult) queryCount.getSingleResult()).toList().iterator().next();
				
				if (request.lazyLoadEvent != null) {
					if (request.lazyLoadEvent.multiSortMeta != null && !request.lazyLoadEvent.multiSortMeta.isEmpty()) {
						request.lazyLoadEvent.multiSortMeta.forEach(sortMeta -> {
							if (sortMeta.toSortDirection().isAscending()) {
								q.addAscendingOrdering(sortMeta.field);
							} else {
								q.addDescendingOrdering(sortMeta.field);
							}
						});
					} else if (request.lazyLoadEvent.sortField != null && !request.lazyLoadEvent.sortField.isEmpty()) {
						if (request.lazyLoadEvent.toSortDirection().isAscending()) {
							q.addAscendingOrdering(request.lazyLoadEvent.sortField);
						} else {
							q.addDescendingOrdering(request.lazyLoadEvent.sortField);
						}
					}
				}

				// Wrap the native query in a standard JPA Query and execute it
				final Query query = JpaHelper.createQuery(q, em);
				final DatabaseQuery dbQuery = ((EJBQueryImpl<?>) query).getDatabaseQuery();

				// load the entitygraph
				if (request.entityGraph != null && !request.entityGraph.isEmpty()) {
					EntityGraph<?> entityGraph = em.getEntityGraph(request.entityGraph);
					if (entityGraph != null) {
						query.setHint("javax.persistence.loadgraph", entityGraph);
					}
				}

				// Do pagination
				//int totalPages = 1;
				Pageable pageable = request.toPageable();
				if (pageable != null) {
					query.setFirstResult((int) pageable.getOffset());
					// Extra one is added to the limit value to check are there more rows or not.
					// It will be removed later on in the response builder.
					query.setMaxResults(pageable.getPageSize() + 1);
//					if (totalRegisters > pageable.getPageSize()) {
//		            		totalPages = (int) Math.ceil((double) totalRegisters / pageable.getPageSize());
//		            }
				}

				final Map<String, Object> queryParams = getQueryParameters(uriInfo);
				// Fields filtering
				if (query.getMaxResults() != Integer.MAX_VALUE) {
					queryParams.put(QueryParameters.JPARS_PAGING_LIMIT, String.valueOf(query.getMaxResults() - 1));
					queryParams.put(QueryParameters.JPARS_PAGING_OFFSET, String.valueOf(query.getFirstResult()));
				}

				// Fields filtering
				FieldsFilter fieldsFilter = null;
				if (context.getSupportedFeatureSet().isSupported(Feature.FIELDS_FILTERING)) {
					final FieldsFilteringValidator fieldsFilteringValidator = new FieldsFilteringValidator(uriInfo);
					if (fieldsFilteringValidator.isFeatureApplicable()) {
						fieldsFilter = fieldsFilteringValidator.getFilter();
					}
				}

				PagingResponseBuilder responseBuilder = new PagingResponseBuilder();

				if (dbQuery instanceof ReportQuery) {
					// simple types selected : select u.name, u.age from employee
					List<ReportItem> reportItems = ((ReportQuery) dbQuery).getItems();
					List<Object[]> queryResults = query.getResultList();
					if ((queryResults != null) && (!queryResults.isEmpty())) {
						Object list = responseBuilder.buildReportQueryResponse(context, queryParams, queryResults, reportItems, uriInfo);
						if (list != null) {
							((PageableCollection)list).setCount(totalRegisters);
							return Response.ok(new StreamingOutputMarshaller(context, list,
									headers.getAcceptableMediaTypes(), fieldsFilter)).build();
						} else {
							// something is wrong with the descriptors
							throw JPARSException.responseCouldNotBeBuiltForNamedQueryRequest(type, context.getName());
						}
					}
					return Response.ok(new StreamingOutputMarshaller(context, queryResults,
							headers.getAcceptableMediaTypes(), fieldsFilter)).build();
				}

				List<Object> results = query.getResultList();
				if (results != null) {
					Object list = responseBuilder.buildReadAllQueryResponse(context, queryParams, results, uriInfo);
					((PageableCollection)list).setCount(totalRegisters);
					return Response.ok(new StreamingOutputMarshaller(context, list, headers.getAcceptableMediaTypes(),
							fieldsFilter)).build();
				}
				return Response.ok(
						new StreamingOutputMarshaller(context, null, headers.getAcceptableMediaTypes(), fieldsFilter))
						.build();

			}

		} catch (Exception ex) {
			throw JPARSException.exceptionOccurred(ex);
		}
	}

	@POST
	@Path("{type}/count")
	@Produces(MediaType.TEXT_PLAIN)
	public Response countByExample(@PathParam("version") String version, @PathParam("context") String persistenceUnit,
			@PathParam("type") String type, PageRequestByExample<?> request, @Context HttpHeaders hh,
			@Context UriInfo ui) {
		setRequestUniqueId();
		return countByExampleInternal(version, persistenceUnit, type, request, hh, ui);
	}

	protected Response countByExampleInternal(String version, String persistenceUnit, String type,
			PageRequestByExample<?> request, HttpHeaders headers, UriInfo uriInfo) {
		JPARSLogger.entering(CLASS_NAME, "countByExampleInternal", new Object[] { "POST", version, persistenceUnit,
				type, request, headers, uriInfo.getRequestUri().toASCIIString() });

		try {
			final PersistenceContext context = getPersistenceContext(persistenceUnit, type, uriInfo.getBaseUri(),
					version, null);
			final Map<String, String> discriminators = getMatrixParameters(uriInfo, persistenceUnit);
			ClassDescriptor descriptor = context.getServerSession().getDescriptorForAlias(type);
			if (descriptor == null) {
				JPARSLogger.error(context.getSessionLog(), "jpars_could_not_find_entity_type",
						new Object[] { type, persistenceUnit });
				throw JPARSException.classOrClassDescriptorCouldNotBeFoundForEntity(type, persistenceUnit);
			} else {
				Class<?> probeType = descriptor.getJavaClass();
				EntityManager em = context.getEmf().createEntityManager(discriminators);

				// Create a native EclipseLink query using QBE policy
				QueryByExamplePolicy policy = new QueryByExamplePolicy();
				policy.excludeDefaultPrimitiveValues();
				policy.addSpecialOperation(String.class, "likeIgnoreCase");

				ObjectMapper mapper = new ObjectMapper();
				Object convertValue = mapper.convertValue(request.example, probeType);
				ReadAllQuery q = new ReadAllQuery(convertValue, policy);

				// query count
				ReportQuery rq = new ReportQuery(q.getExpressionBuilder());
				rq.addCount();
				rq.setExampleObject(convertValue);
				Query queryCount = JpaHelper.createQuery(rq, em);
				Integer count = (Integer) ((ReportQueryResult) queryCount.getSingleResult()).toList().iterator().next();

				return Response.ok(count).build();
			}

		} catch (Exception ex) {
			throw JPARSException.exceptionOccurred(ex);
		}
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_OCTET_STREAM })
    @Path("{type}/{id}/{attribute}/download")
    public Response findAttributeDownload(@PathParam("version") String version,
                                      @PathParam("context") String persistenceUnit,
                                      @PathParam("type") String type,
                                      @PathParam("id") String id,
                                      @PathParam("attribute") String attribute,
                                      @Context HttpHeaders hh,
                                      @Context UriInfo ui) {
        setRequestUniqueId();
        return findAttributeDownloadInternal(version, persistenceUnit, type, id, attribute, hh, ui);
    }
	
	protected Response findAttributeDownloadInternal(String version, String persistenceUnit, String type, String id, String attribute, HttpHeaders headers, UriInfo uriInfo) {
		JPARSLogger.entering(CLASS_NAME, "findAttributeDownloadInternal", new Object[] { "GET", version, persistenceUnit, type, id, attribute, uriInfo.getRequestUri().toASCIIString() });

        EntityManager em = null;
        try {
            PersistenceContext context = getPersistenceContext(persistenceUnit, type, uriInfo.getBaseUri(), version, null);
            Object entityId = IdHelper.buildId(context, type, id);
            em = context.getEmf().createEntityManager(getMatrixParameters(uriInfo, persistenceUnit));

            Object entity = em.find(context.getClass(type), entityId, getQueryParameters(uriInfo));
            DatabaseSession serverSession = context.getServerSession();
            ClassDescriptor descriptor = serverSession.getClassDescriptor(context.getClass(type));
            if (descriptor == null) {
                throw JPARSException.classOrClassDescriptorCouldNotBeFoundForEntity(type, persistenceUnit);
            }

            String attrPrefix = attribute.substring(0, attribute.indexOf("Binary"));
            DatabaseMapping mappingBinary = descriptor.getMappingForAttributeName(attribute);
            DatabaseMapping mappingContentType = descriptor.getMappingForAttributeName(attrPrefix + "ContentType");
            DatabaseMapping mappingFileName = descriptor.getMappingForAttributeName(attrPrefix + "FileName");
            if ((mappingBinary == null) || (entity == null)) {
                throw JPARSException.databaseMappingCouldNotBeFoundForEntityAttribute(attribute, type, id, persistenceUnit);
            }

            if (!mappingBinary.isDirectToFieldMapping()) {
            	throw JPARSException.databaseMappingCouldNotBeFoundForEntityAttribute(attribute, type, id, persistenceUnit);
            } else {
            	
            	byte[] resultBinary = (byte[]) mappingBinary.getRealAttributeValueFromAttribute(mappingBinary.getAttributeValueFromObject(entity), entity, (AbstractSession) serverSession);
            	if (resultBinary != null) {
            		String resultContentType = (String) mappingContentType.getRealAttributeValueFromAttribute(mappingContentType.getAttributeValueFromObject(entity), entity, (AbstractSession) serverSession);
            		String resultFileName = (String) mappingFileName.getRealAttributeValueFromAttribute(mappingFileName.getAttributeValueFromObject(entity), entity, (AbstractSession) serverSession);
                	StreamingOutput stream = new StreamingOutput() {
                        @Override
                        public void write(OutputStream os) throws IOException, WebApplicationException {
                        	BufferedOutputStream baos = new BufferedOutputStream(os);
                        	baos.write((byte[]) resultBinary);
                            baos.flush();
                        }
                    };
                    return Response.ok(stream, resultContentType)
                    		.header("Content-Disposition", "attachment; filename=\""+resultFileName+"\"")
                    		.build();
            	} else {
            		return Response.noContent().build();
            	}
            }
            
        } catch (Exception ex) {
            throw JPARSException.exceptionOccurred(ex);
        } finally {
            if (em != null) {
                if (em.isOpen()) {
                    em.close();
                }
            }
        }
	}
	
	@POST
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Path("{type}/{id}/{attribute}/upload")
    public Response setAttributeFromUpload(@PathParam("version") String version,
                                      @PathParam("context") String persistenceUnit,
                                      @PathParam("type") String type,
                                      @PathParam("id") String id,
                                      @PathParam("attribute") String attribute,
                                      @Context HttpHeaders hh,
                                      @Context UriInfo ui,
                                      MultipartFormDataInput input) {
        setRequestUniqueId();
        return setAttributeFromUploadInternal(version, persistenceUnit, type, id, attribute, hh, ui, input);
    }
	
	/**
	 * @param version
	 * @param persistenceUnit
	 * @param type
	 * @param id
	 * @param attribute
	 * @param headers
	 * @param uriInfo
	 * @param input
	 * @return
	 */
	protected Response setAttributeFromUploadInternal(String version, String persistenceUnit, String type, String id, String attribute, HttpHeaders headers, UriInfo uriInfo, MultipartFormDataInput input) {
        JPARSLogger.entering(CLASS_NAME, "setAttributeFromUploadInternal", new Object[] { "POST", headers.getMediaType(), version, persistenceUnit, type, id, attribute, uriInfo.getRequestUri().toASCIIString() });
        InputStream inStream = null;
        ByteArrayOutputStream baos = null;
        try {
            PersistenceContext context = getPersistenceContext(persistenceUnit, type, uriInfo.getBaseUri(), version, null);
            Object entityId = IdHelper.buildId(context, type, id);
			Map<String, String> discriminators = getMatrixParameters(uriInfo, attribute);
			EntityManager em = context.getEmf().createEntityManager(discriminators);
            ClassDescriptor descriptor = context.getDescriptor(type);
            
            String attrPrefix = attribute.substring(0, attribute.indexOf("Binary"));
            DatabaseMapping mappingBinary = descriptor.getMappingForAttributeName(attribute);
            DatabaseMapping mappingFileName = descriptor.getMappingForAttributeName(attrPrefix + "FileName");
            DatabaseMapping mappingContentType = descriptor.getMappingForAttributeName(attrPrefix + "ContentType");
            DatabaseMapping mappingSize = descriptor.getMappingForAttributeName(attrPrefix + "Size");
            
            if (!mappingBinary.isDirectToFieldMapping()) {
                JPARSLogger.error(context.getSessionLog(), "jpars_could_not_find_appropriate_mapping_for_update", new Object[] { attribute, type, id, persistenceUnit });
                throw JPARSException.databaseMappingCouldNotBeFoundForEntityAttribute(attribute, type, id, persistenceUnit);
            }
            
            List<InputPart> parts = input.getFormDataMap().get(attribute);
            Response response = null;
            for (InputPart inPart : parts) {
				try {
					MultivaluedMap<String, String> pHeaders = inPart.getHeaders();
					String fileName = parseFileName(pHeaders);
					String contentType = parseContentType(pHeaders);
					inStream = inPart.getBody(InputStream.class,null);
					baos = new ByteArrayOutputStream();
		            byte[] buf = new byte[8192];
		            int read = -1;
		            while ((read = inStream.read(buf)) > -1) {
		            		baos.write(buf, 0, read);
		            }
		            baos.flush();
		            byte[] binaryData = baos.toByteArray();
		            Long size = Long.valueOf(binaryData.length);
		            
		            TransactionWrapper transaction = getTransactionWrapper(context);
		            transaction.beginTransaction(em);
		            try {
		            	Object entity = context.find(discriminators, type, entityId, getQueryParameters(uriInfo));
	                    if (entity == null) {
	                        return null;
	                    }
	                    ((DirectToFieldMapping) mappingBinary).setAttributeValueInObject(entity, binaryData);
	                    ((DirectToFieldMapping) mappingFileName).setAttributeValueInObject(entity, fileName);
	                    ((DirectToFieldMapping) mappingContentType).setAttributeValueInObject(entity, contentType);
	                    ((DirectToFieldMapping) mappingSize).setAttributeValueInObject(entity, size);
	                    
	                    transaction.commitTransaction(em);
	                    
			            response = Response.ok().build();
	                } catch (RollbackException e) {
	                    JPARSLogger.exception(context.getSessionLog(), "exception_while_updating_attribute", new Object[] { type, attribute }, e);
	                    return null;
	                } catch (Exception e) {
	                    JPARSLogger.exception(context.getSessionLog(), "exception_while_updating_attribute", new Object[] { type, attribute }, e);
	                    transaction.rollbackTransaction(em);
	                    return null;
	                }
				} catch (Exception e) {
					throw JPARSException.exceptionOccurred(e);
				}
			}
            if (response == null) {
            	response = Response.status(Status.BAD_REQUEST).build();
            }
            return response;
        } catch (Exception ex) {
            throw JPARSException.exceptionOccurred(ex);
        } finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
				}
			}
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
				}
			}
		}
    }
	
	private String parseFileName(MultivaluedMap<String, String> headers) {
		String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");
		for (String name : contentDispositionHeader) {
			if ((name.trim().startsWith("filename"))) {
				String[] tmp = name.split("=");
				String fileName = tmp[1].trim().replaceAll("\"", "");
				return fileName;
			}
		}
		return "randomName";
	}
	
	private String parseContentType(MultivaluedMap<String, String> headers) {
		return headers.getFirst("Content-Type");
	}

}