package org.eclipse.persistence.jpa.rs.cutsom.resources.util;

import org.springframework.data.domain.Pageable;

public class PageRequestByExample<E> {
	
	public E example;
	public LazyLoadEvent lazyLoadEvent;
	public String entityGraph;

	public Pageable toPageable() {
		return lazyLoadEvent != null ? lazyLoadEvent.toPageable() : null;
	}
}