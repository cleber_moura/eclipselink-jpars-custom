package org.eclipse.persistence.jpa.rs.cutsom.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import org.eclipse.persistence.jpa.rs.DataStorage;
import org.eclipse.persistence.jpa.rs.PersistenceContextFactory;
import org.eclipse.persistence.jpa.rs.PersistenceContextFactoryProvider;
import org.eclipse.persistence.jpa.rs.exceptions.JPARSExceptionMapper;
import org.eclipse.persistence.jpa.rs.service.JPARSApplication;

/**
 * Customização do {@link JPARSApplication} para registro de novo endpoint.
 * 
 * @author clebertmoura
 *
 */
@ApplicationPath("/jparsapi/")
public class CustomJPARSApplication extends JPARSApplication {

	private final Set<Class<?>> classes;

    /**
     * Instantiates a new jPARS application.
     */
    @SuppressWarnings("deprecation")
	public CustomJPARSApplication() {
        //
        HashSet<Class<?>> c = new HashSet<Class<?>>();

        // Unversioned Resources (resources that do not have version in the url)
        c.add(org.eclipse.persistence.jpa.rs.resources.unversioned.PersistenceResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.unversioned.PersistenceUnitResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.unversioned.EntityResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.unversioned.SingleResultQueryResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.unversioned.QueryResource.class);

        // Versioned Resources (resources that do have version in the url)
        c.add(org.eclipse.persistence.jpa.rs.resources.PersistenceResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.PersistenceUnitResource.class);
        // comentado por clebertmoura
        //c.add(org.eclipse.persistence.jpa.rs.resources.EntityResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.SingleResultQueryResource.class);
        c.add(org.eclipse.persistence.jpa.rs.resources.QueryResource.class);
        
        //custom por clebertmoura
        c.add(org.eclipse.persistence.jpa.rs.cutsom.resources.CustomEntityResource.class);

        // JPARS 2.0
        c.add(org.eclipse.persistence.jpa.rs.resources.MetadataResource.class);

        // Exception Mapping
        c.add(JPARSExceptionMapper.class);

        //
        classes = Collections.unmodifiableSet(c);
    }

    /* (non-Javadoc)
     * @see javax.ws.rs.core.Application#getClasses()
     */
    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    /**
     * Clean up.
     */
    @PreDestroy
    public void preDestroy() {
        DataStorage.destroy();

        ServiceLoader<PersistenceContextFactoryProvider> persistenceContextFactoryProviderLoader =
                ServiceLoader.load(PersistenceContextFactoryProvider.class, Thread.currentThread().getContextClassLoader());

        for (PersistenceContextFactoryProvider persistenceContextFactoryProvider : persistenceContextFactoryProviderLoader) {
            PersistenceContextFactory persistenceContextFactory = persistenceContextFactoryProvider.getPersistenceContextFactory(null);
            if (persistenceContextFactory != null) {
                persistenceContextFactory.close();
            }
        }
    }

}
